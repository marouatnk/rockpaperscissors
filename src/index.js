document.addEventListener("DOMContentLoaded", function(event) {
    //console.log("DOM fully loaded and parsed");

    function Game(){
		this.computerInput = function(){
			this.computerChoice = Math.random();
			if (this.computerChoice < 0.34) {
				return this.computerChoice = "rock";
			} else if(this.computerChoice <= 0.67) {
				return this.computerChoice = "paper";
			} else {
				return this.computerChoice = "scissors";
			};
		};

		this.compare = function(userchoice, computerChoice){
			// choice 1 = userChoice
			// choice 2 = computerChoice
			if(userchoice === computerChoice){
		        return "Draw!";  
		    }
		    else if(userchoice === "rock"){
		        if(computerChoice === "scissors"){
		        	scoreCount('add');
		            return "rock wins";   
		            
		        } else {
		        	scoreCount('minus'); 
		            return "paper wins";
		             
		        }
		    }
		    else if(userchoice === "paper"){
		        if(computerChoice === "rock"){
		        	scoreCount('add');
		            return "paper wins";   
		            
		        } else {
		        	scoreCount('minus'); 
		            return "scissors wins"; 
		               
		        }
		    }
		    else if(userchoice === "scissors"){
		        if(computerChoice === "rock"){
		        	scoreCount('add');
		            return "rock wins";   
		            
		        } else {
		        	scoreCount('minus'); 
		            return "scissors wins";  
		              
		        }
		    }
		};
	};

	function symbol(choice) {
		if(choice == "paper"){
			return '<img src="./images/paper.png" />';
		}
		else if(choice == "scissors"){
			return '<img src="./images/scissors.jpg" />';
		}
		else {
			return '<img src="./images/rock.png" />';
		}
	}

	function scoreCount(newScore){
		if(newScore == "add"){
			score = score+1;
		} else {
			if(score > 0){
				score = score-1;
			}
		}
		scoreDisplay.innerHTML = "Your score: "+score;
	}


	let game = new Game();
	let userChoice = "";
	let computerChoice = "";
	let result = "";
	let buttons = document.getElementsByClassName("button");
	let playerChoiceDisplay = document.getElementById("player_choice");
	let computerChoiceDisplay = document.getElementById("computer_choice");
	let resultDisplay = document.getElementById("result");
	let score = 0;
	let scoreDisplay = document.getElementById("score_display");
    for (let i = 0; i < buttons.length; i++) {
	    buttons[i].addEventListener('click', function(){
			userChoice = this.id;
			let choiceGraphic = symbol(userChoice);
			playerChoiceDisplay.innerHTML = choiceGraphic;
	//console.log("user: " + userChoice);
			computerChoice = game.computerInput();
			 choiceGraphic = symbol(computerChoice);
			computerChoiceDisplay.innerHTML = choiceGraphic;
	//console.log("computer: " + computerChoice);
			result = game.compare(userChoice, computerChoice);
	//console.log(result);
			resultDisplay.innerHTML = result;
		}, false);
	}
	
});