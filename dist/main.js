/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

document.addEventListener("DOMContentLoaded", function(event) {
    //console.log("DOM fully loaded and parsed");

    function Game(){
		this.computerInput = function(){
			this.computerChoice = Math.random();
			if (this.computerChoice < 0.34) {
				return this.computerChoice = "rock";
			} else if(this.computerChoice <= 0.67) {
				return this.computerChoice = "paper";
			} else {
				return this.computerChoice = "scissors";
			};
		};

		this.compare = function(userchoice, computerChoice){
			// choice 1 = userChoice
			// choice 2 = computerChoice
			if(userchoice === computerChoice){
		        return "Draw!";  
		    }
		    else if(userchoice === "rock"){
		        if(computerChoice === "scissors"){
		        	scoreCount('add');
		            return "rock wins";   
		            
		        } else {
		        	scoreCount('minus'); 
		            return "paper wins";
		             
		        }
		    }
		    else if(userchoice === "paper"){
		        if(computerChoice === "rock"){
		        	scoreCount('add');
		            return "paper wins";   
		            
		        } else {
		        	scoreCount('minus'); 
		            return "scissors wins"; 
		               
		        }
		    }
		    else if(userchoice === "scissors"){
		        if(computerChoice === "rock"){
		        	scoreCount('add');
		            return "rock wins";   
		            
		        } else {
		        	scoreCount('minus'); 
		            return "scissors wins";  
		              
		        }
		    }
		};
	};

	function symbol(choice) {
		if(choice == "paper"){
			return '<img src="./images/paper.png" />';
		}
		else if(choice == "scissors"){
			return '<img src="./images/scissors.jpg" />';
		}
		else {
			return '<img src="./images/rock.png" />';
		}
	}

	function scoreCount(newScore){
		if(newScore == "add"){
			score = score+1;
		} else {
			if(score > 0){
				score = score-1;
			}
		}
		scoreDisplay.innerHTML = "Your score: "+score;
	}


	let game = new Game();
	let userChoice = "";
	let computerChoice = "";
	let result = "";
	let buttons = document.getElementsByClassName("button");
	let playerChoiceDisplay = document.getElementById("player_choice");
	let computerChoiceDisplay = document.getElementById("computer_choice");
	let resultDisplay = document.getElementById("result");
	let score = 0;
	let scoreDisplay = document.getElementById("score_display");
    for (let i = 0; i < buttons.length; i++) {
	    buttons[i].addEventListener('click', function(){
			userChoice = this.id;
			let choiceGraphic = symbol(userChoice);
			playerChoiceDisplay.innerHTML = choiceGraphic;
	//console.log("user: " + userChoice);
			computerChoice = game.computerInput();
			 choiceGraphic = symbol(computerChoice);
			computerChoiceDisplay.innerHTML = choiceGraphic;
	//console.log("computer: " + computerChoice);
			result = game.compare(userChoice, computerChoice);
	//console.log(result);
			resultDisplay.innerHTML = result;
		}, false);
	}
	
});

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDOztBQUVBLFdBQVc7QUFDWCwrQjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQzs7QUFFQSxXQUFXO0FBQ1gsK0I7QUFDQSxxQzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUM7O0FBRUEsV0FBVztBQUNYLCtCO0FBQ0EscUM7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQixvQkFBb0I7QUFDdkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOztBQUVBLENBQUMsRSIsImZpbGUiOiJtYWluLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9zcmMvaW5kZXguanNcIik7XG4iLCJkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKFwiRE9NQ29udGVudExvYWRlZFwiLCBmdW5jdGlvbihldmVudCkge1xuICAgIC8vY29uc29sZS5sb2coXCJET00gZnVsbHkgbG9hZGVkIGFuZCBwYXJzZWRcIik7XG5cbiAgICBmdW5jdGlvbiBHYW1lKCl7XG5cdFx0dGhpcy5jb21wdXRlcklucHV0ID0gZnVuY3Rpb24oKXtcblx0XHRcdHRoaXMuY29tcHV0ZXJDaG9pY2UgPSBNYXRoLnJhbmRvbSgpO1xuXHRcdFx0aWYgKHRoaXMuY29tcHV0ZXJDaG9pY2UgPCAwLjM0KSB7XG5cdFx0XHRcdHJldHVybiB0aGlzLmNvbXB1dGVyQ2hvaWNlID0gXCJyb2NrXCI7XG5cdFx0XHR9IGVsc2UgaWYodGhpcy5jb21wdXRlckNob2ljZSA8PSAwLjY3KSB7XG5cdFx0XHRcdHJldHVybiB0aGlzLmNvbXB1dGVyQ2hvaWNlID0gXCJwYXBlclwiO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0cmV0dXJuIHRoaXMuY29tcHV0ZXJDaG9pY2UgPSBcInNjaXNzb3JzXCI7XG5cdFx0XHR9O1xuXHRcdH07XG5cblx0XHR0aGlzLmNvbXBhcmUgPSBmdW5jdGlvbih1c2VyY2hvaWNlLCBjb21wdXRlckNob2ljZSl7XG5cdFx0XHQvLyBjaG9pY2UgMSA9IHVzZXJDaG9pY2Vcblx0XHRcdC8vIGNob2ljZSAyID0gY29tcHV0ZXJDaG9pY2Vcblx0XHRcdGlmKHVzZXJjaG9pY2UgPT09IGNvbXB1dGVyQ2hvaWNlKXtcblx0XHQgICAgICAgIHJldHVybiBcIkRyYXchXCI7ICBcblx0XHQgICAgfVxuXHRcdCAgICBlbHNlIGlmKHVzZXJjaG9pY2UgPT09IFwicm9ja1wiKXtcblx0XHQgICAgICAgIGlmKGNvbXB1dGVyQ2hvaWNlID09PSBcInNjaXNzb3JzXCIpe1xuXHRcdCAgICAgICAgXHRzY29yZUNvdW50KCdhZGQnKTtcblx0XHQgICAgICAgICAgICByZXR1cm4gXCJyb2NrIHdpbnNcIjsgICBcblx0XHQgICAgICAgICAgICBcblx0XHQgICAgICAgIH0gZWxzZSB7XG5cdFx0ICAgICAgICBcdHNjb3JlQ291bnQoJ21pbnVzJyk7IFxuXHRcdCAgICAgICAgICAgIHJldHVybiBcInBhcGVyIHdpbnNcIjtcblx0XHQgICAgICAgICAgICAgXG5cdFx0ICAgICAgICB9XG5cdFx0ICAgIH1cblx0XHQgICAgZWxzZSBpZih1c2VyY2hvaWNlID09PSBcInBhcGVyXCIpe1xuXHRcdCAgICAgICAgaWYoY29tcHV0ZXJDaG9pY2UgPT09IFwicm9ja1wiKXtcblx0XHQgICAgICAgIFx0c2NvcmVDb3VudCgnYWRkJyk7XG5cdFx0ICAgICAgICAgICAgcmV0dXJuIFwicGFwZXIgd2luc1wiOyAgIFxuXHRcdCAgICAgICAgICAgIFxuXHRcdCAgICAgICAgfSBlbHNlIHtcblx0XHQgICAgICAgIFx0c2NvcmVDb3VudCgnbWludXMnKTsgXG5cdFx0ICAgICAgICAgICAgcmV0dXJuIFwic2Npc3NvcnMgd2luc1wiOyBcblx0XHQgICAgICAgICAgICAgICBcblx0XHQgICAgICAgIH1cblx0XHQgICAgfVxuXHRcdCAgICBlbHNlIGlmKHVzZXJjaG9pY2UgPT09IFwic2Npc3NvcnNcIil7XG5cdFx0ICAgICAgICBpZihjb21wdXRlckNob2ljZSA9PT0gXCJyb2NrXCIpe1xuXHRcdCAgICAgICAgXHRzY29yZUNvdW50KCdhZGQnKTtcblx0XHQgICAgICAgICAgICByZXR1cm4gXCJyb2NrIHdpbnNcIjsgICBcblx0XHQgICAgICAgICAgICBcblx0XHQgICAgICAgIH0gZWxzZSB7XG5cdFx0ICAgICAgICBcdHNjb3JlQ291bnQoJ21pbnVzJyk7IFxuXHRcdCAgICAgICAgICAgIHJldHVybiBcInNjaXNzb3JzIHdpbnNcIjsgIFxuXHRcdCAgICAgICAgICAgICAgXG5cdFx0ICAgICAgICB9XG5cdFx0ICAgIH1cblx0XHR9O1xuXHR9O1xuXG5cdGZ1bmN0aW9uIHN5bWJvbChjaG9pY2UpIHtcblx0XHRpZihjaG9pY2UgPT0gXCJwYXBlclwiKXtcblx0XHRcdHJldHVybiAnPGltZyBzcmM9XCIuL2ltYWdlcy9wYXBlci5wbmdcIiAvPic7XG5cdFx0fVxuXHRcdGVsc2UgaWYoY2hvaWNlID09IFwic2Npc3NvcnNcIil7XG5cdFx0XHRyZXR1cm4gJzxpbWcgc3JjPVwiLi9pbWFnZXMvc2Npc3NvcnMuanBnXCIgLz4nO1xuXHRcdH1cblx0XHRlbHNlIHtcblx0XHRcdHJldHVybiAnPGltZyBzcmM9XCIuL2ltYWdlcy9yb2NrLnBuZ1wiIC8+Jztcblx0XHR9XG5cdH1cblxuXHRmdW5jdGlvbiBzY29yZUNvdW50KG5ld1Njb3JlKXtcblx0XHRpZihuZXdTY29yZSA9PSBcImFkZFwiKXtcblx0XHRcdHNjb3JlID0gc2NvcmUrMTtcblx0XHR9IGVsc2Uge1xuXHRcdFx0aWYoc2NvcmUgPiAwKXtcblx0XHRcdFx0c2NvcmUgPSBzY29yZS0xO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRzY29yZURpc3BsYXkuaW5uZXJIVE1MID0gXCJZb3VyIHNjb3JlOiBcIitzY29yZTtcblx0fVxuXG5cblx0bGV0IGdhbWUgPSBuZXcgR2FtZSgpO1xuXHRsZXQgdXNlckNob2ljZSA9IFwiXCI7XG5cdGxldCBjb21wdXRlckNob2ljZSA9IFwiXCI7XG5cdGxldCByZXN1bHQgPSBcIlwiO1xuXHRsZXQgYnV0dG9ucyA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoXCJidXR0b25cIik7XG5cdGxldCBwbGF5ZXJDaG9pY2VEaXNwbGF5ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJwbGF5ZXJfY2hvaWNlXCIpO1xuXHRsZXQgY29tcHV0ZXJDaG9pY2VEaXNwbGF5ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJjb21wdXRlcl9jaG9pY2VcIik7XG5cdGxldCByZXN1bHREaXNwbGF5ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJyZXN1bHRcIik7XG5cdGxldCBzY29yZSA9IDA7XG5cdGxldCBzY29yZURpc3BsYXkgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInNjb3JlX2Rpc3BsYXlcIik7XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBidXR0b25zLmxlbmd0aDsgaSsrKSB7XG5cdCAgICBidXR0b25zW2ldLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24oKXtcblx0XHRcdHVzZXJDaG9pY2UgPSB0aGlzLmlkO1xuXHRcdFx0bGV0IGNob2ljZUdyYXBoaWMgPSBzeW1ib2wodXNlckNob2ljZSk7XG5cdFx0XHRwbGF5ZXJDaG9pY2VEaXNwbGF5LmlubmVySFRNTCA9IGNob2ljZUdyYXBoaWM7XG5cdC8vY29uc29sZS5sb2coXCJ1c2VyOiBcIiArIHVzZXJDaG9pY2UpO1xuXHRcdFx0Y29tcHV0ZXJDaG9pY2UgPSBnYW1lLmNvbXB1dGVySW5wdXQoKTtcblx0XHRcdCBjaG9pY2VHcmFwaGljID0gc3ltYm9sKGNvbXB1dGVyQ2hvaWNlKTtcblx0XHRcdGNvbXB1dGVyQ2hvaWNlRGlzcGxheS5pbm5lckhUTUwgPSBjaG9pY2VHcmFwaGljO1xuXHQvL2NvbnNvbGUubG9nKFwiY29tcHV0ZXI6IFwiICsgY29tcHV0ZXJDaG9pY2UpO1xuXHRcdFx0cmVzdWx0ID0gZ2FtZS5jb21wYXJlKHVzZXJDaG9pY2UsIGNvbXB1dGVyQ2hvaWNlKTtcblx0Ly9jb25zb2xlLmxvZyhyZXN1bHQpO1xuXHRcdFx0cmVzdWx0RGlzcGxheS5pbm5lckhUTUwgPSByZXN1bHQ7XG5cdFx0fSwgZmFsc2UpO1xuXHR9XG5cdFxufSk7Il0sInNvdXJjZVJvb3QiOiIifQ==